
<?php
function foo()
{
    static $bar;
    $bar++;
    echo "Before unset: $bar, ";
    unset($bar);
    $bar = 4;
    echo "after unset: $bar\n"."<br>";
}

foo();
foo();
foo();
foo();
foo();
?>
